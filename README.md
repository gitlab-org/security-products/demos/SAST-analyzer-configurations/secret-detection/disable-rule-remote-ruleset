# Remote ruleset that disables a rule from default ruleset

This project includes a [ruleset configuration](.gitlab/secret-detection-ruleset.toml) that disables a rule from pipeline secret detection's [default ruleset](https://gitlab.com/gitlab-org/security-products/analyzers/secrets/-/blob/master/gitleaks.toml).

Check the [project](https://gitlab.com/gitlab-org/security-products/demos/analyzer-configurations/secret-detection/modify-default-ruleset/remote-ruleset/disable-rule-project) using this ruleset configuration for more details.
